// Copyright 2022-2023 Duncan McIntosh <duncan82013@live.ca>
//
// This file is part of the Sigmund project.
// See <https://codeberg.org/duncanm/sigmund>.
//
// SPDX-License-Identifier: GPL-3.0-only
const isIterable = (lst) => lst && typeof lst[Symbol.iterator] === 'function';

sigmund.handleEvent('tab changed', null, async (info) => {
    return checkActiveTabs();
});

sigmund.handleEvent('tab updated', { properties: ['url'] }, async (details) => {
    return checkActiveTabs();
});

sigmund.handleEvent('alarm', null, async (info) => {
    if (info.name === 'tick' || info.name === 'tick (for popup)') {
        return checkActiveTabs();
    }

    if (info.name === 'notify') {
        const remaining = await checkActiveTabs(true);
        return sendWarningNotification(remaining);
    }
});

async function checkActiveTabs(scheduleDelayedNotification) {
    const res = await Promise.all([
        sigmund.get('sync', [
            'remaining',
            'expiry',
            'blocklist',
            'notifyTimes',
            'enableNotifications',
        ]),
        sigmund.get('session', ['lastTime']),
        sigmund.getActiveTabs(),
    ]);

    const { remaining, expiry, blocklist, notifyTimes, enableNotifications } =
        res[0];
    const { lastTime } = res[1];
    const activeTabs = res[2];

    const newRemaining = await updateTimes(remaining, expiry, lastTime);

    const trespassingTabs = activeTabs.filter((tab) =>
        shouldBeBlocked(blocklist, tab.url)
    );
    const anyTabsTrespassing = trespassingTabs.length > 0;

    let reactions = [];
    if (anyTabsTrespassing && newRemaining > 0) {
        reactions.push(trespassing(newRemaining));

        const delayed = newRemaining + (scheduleDelayedNotification ? -0.1 : 0);
        const nextNotificationDelay = nextNotificationTime(
            notifyTimes,
            delayed
        );
        if (nextNotificationDelay !== null && enableNotifications) {
            reactions.push(sigmund.setAlarm('notify', nextNotificationDelay));
        }
    } else {
        reactions.push(notTrespassing(newRemaining));
    }

    if (newRemaining <= 0) {
        let tabIds = trespassingTabs.map((tab) => tab.id);
        reactions.push(showBlockPageOnTabs(tabIds));
    }

    return Promise.all(reactions).then(() => newRemaining);
}

function shouldBeBlocked(blocklist, urlString) {
    if (urlString === null) {
        return false;
    }

    if (!isIterable(blocklist)) {
        // The blocklist isn't valid, so assume nothing is blocked.
        return false;
    }

    let url;
    try {
        url = new URL(urlString);
    } catch (err) {
        if (typeof err !== TypeError) {
            throw err;
        }

        // Assume it's probably OK.
        console.warn(`Error parsing '${urlString} as a URL: ${err.message}`);
        return false;
    }

    if (!url.protocol.startsWith('http')) {
        // We don't want to accidentally mess with the browser's
        // settings, etc. As such, only work on Web pages.
        return false;
    }

    for (const entry of blocklist) {
        if (url.host === entry) {
            return true;
        }
    }

    return false;
}

async function trespassing(remaining) {
    return Promise.all([
        sigmund.setAlarm('tick', remaining),
        sigmund.set('session', {
            lastTime: sigmund.currentSecondsSinceEpoch(),
        }),
        sigmund.set('sync', { remaining }),
        updateIcon(true, remaining),
    ]);
}

async function notTrespassing(remaining) {
    return Promise.all([
        sigmund.clearAlarm('tick'),
        sigmund.clearAlarm('notify'),
        sigmund.set('session', {
            lastTime: undefined,
        }),
        sigmund.set('sync', { remaining }),
        updateIcon(false, remaining),
    ]);
}

async function updateTimes(remaining, expiry, lastTime) {
    if (
        typeof remaining !== 'number' ||
        typeof expiry !== 'number' ||
        sigmund.currentSecondsSinceEpoch() > expiry
    ) {
        return resetRemainingTime();
    }

    if (typeof lastTime === 'number') {
        // Subtract the time since we last checked.
        remaining -= sigmund.currentSecondsSinceEpoch() - lastTime;
    }

    return remaining;
}

async function resetRemainingTime() {
    // This doesn't happen very often, so we don't try and coalesce
    // these into one call.
    let { totalTime } = await sigmund.get('sync', ['totalTime']);
    if (typeof totalTime !== 'number') {
        // Set an hour as the default.
        totalTime = 60 * 60;
        await sigmund.set('sync', { totalTime });
    }

    await Promise.all([
        sigmund.set('sync', {
            remaining: totalTime,
            expiry: sigmund.secondsSinceEpochAtTimeTomorrow(4, 0, 0),
        }),
        sigmund.set('session', {
            lastTime: undefined,
        }),
    ]);

    return totalTime;
}

function showBlockPageOnTabs(ids) {
    const blockPageURL = sigmund.getExtensionPage('src/block.html');

    return Promise.all(ids.map((id) => sigmund.navigateTab(id, blockPageURL)));
}

function updateIcon(anyTrespassingTabs, remaining) {
    if (remaining <= 0) {
        return sigmund.setPopupIcon('art/out-of-time.svg');
    }

    if (anyTrespassingTabs) {
        return sigmund.setPopupIcon('art/trespassing.svg');
    }

    return sigmund.setPopupIcon('art/all-good.svg');
}

/**
 * Selects the best time to display the next notification given a list
 * of times in notifyList.
 *
 * @example
 * nextNotificationTime([5, 10, 20], 15)
 * // 10
 * nextNotificationTime([5, 10, 20], 6)
 * // 5
 * nextNotificationTime([5, 10, 20], 1)
 * // null
 *
 * @param {array} notifyList - a list of times in seconds that
 * notifications should be displayed at
 * @param {number} remaining - the remaining duration of time
 *
 * @returns the optimal notification time, or null if no notification
 * should be displayed.
 */
function nextNotificationTime(notifyTimes, remaining) {
    let bestDifference = Infinity;

    if (!isIterable(notifyTimes)) {
        // Assume notifications are disabled.
        return null;
    }

    for (const time of notifyTimes) {
        let difference = remaining - time;

        // Find the one that will fire in the least time.
        if (difference >= 0 && difference < bestDifference) {
            bestDifference = difference;
        }
    }

    if (Number.isFinite(bestDifference)) {
        return bestDifference;
    }

    return null;
}

async function sendWarningNotification(remaining) {
    const currentTime = sigmund.currentSecondsSinceEpoch();

    // TODO translation
    const title = sigmund.i18n('notificationWarningTitle');

    remaining = Math.ceil(remaining);

    const minutes = Math.floor(remaining / 60).toString();
    const seconds = Math.round(remaining % 60)
        .toString()
        .padStart(2, '0');
    const message = sigmund.i18n('notificationWarningBody', [minutes, seconds]);

    return sigmund.sendNotification(title, message, currentTime);
}

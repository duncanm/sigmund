// Copyright 2022 Duncan McIntosh <duncan82013@live.ca>
//
// This file is part of the Sigmund project.
// See <https://codeberg.org/duncanm/sigmund>.
//
// SPDX-License-Identifier: GPL-3.0-only
sigmund.handleEvent('alarm', null, async (info) => {
    if (info.name === 'tick (for popup)') {
        sigmund.setAlarm('tick (for popup)', 1);
    }
});

sigmund.handleEvent('storage updated', null, async (changes, area) => {
    if (typeof changes.remaining === 'undefined') {
        return;
    }

    await updateDisplayedTime(changes.remaining.newValue);
});

document.getElementById('settings-link').addEventListener('click', (e) => {
    e.preventDefault();
    sigmund.showSettingsPage();
});

async function updateDisplayedTime(newTime) {
    const clock = document.getElementById('clock');

    if (newTime <= 0) {
        clock.textContent = '00:00:00';
        return;
    }

    let seconds = (Math.floor(newTime) % 60).toString().padStart(2, '0');
    let minutes = (Math.floor(newTime / 60) % 60).toString().padStart(2, '0');
    let hours = Math.floor(newTime / 3600)
        .toString()
        .padStart(2, '0');

    if (hours < 0 || hours > 99) {
        hours = '!!';
    }

    clock.textContent = `${hours}:${minutes}:${seconds}`;
}

sigmund.setAlarm('tick (for popup)', 0);

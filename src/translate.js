// Copyright 2022 Duncan McIntosh <duncan82013@live.ca>
//
// This file is part of the Sigmund project.
// See <https://codeberg.org/duncanm/sigmund>.
//
// SPDX-License-Identifier: GPL-3.0-only

// Annoyingly, you can't translate HTML declaratively in a
// WebExtension. We hack around it here. (Note: this will translate
// everything BEFORE, and nothing after.)
document.documentElement.setAttribute('lang', browser.i18n.getUILanguage());

for (const element of document.querySelectorAll('[data-i18n]')) {
    element.textContent = browser.i18n.getMessage(
        element.getAttribute('data-i18n')
    );
}

for (const element of document.querySelectorAll('[data-i18n-value]')) {
    element.value = browser.i18n.getMessage(
        element.getAttribute('data-i18n-value')
    );
}

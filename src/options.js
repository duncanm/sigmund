// Copyright 2022-2023 Duncan McIntosh <duncan82013@live.ca>
//
// This file is part of the Sigmund project.
// See <https://codeberg.org/duncanm/sigmund>.
//
// SPDX-License-Identifier: GPL-3.0-only
for (const element of document.querySelectorAll('[data-save]')) {
    element.addEventListener('click', saveSettings);
}

for (const element of document.querySelectorAll('input[data-modifies]')) {
    element.addEventListener('keydown', (e) => fieldChanged(e.target));
}

sigmund.get('sync', ['totalTime']).then((results) => {
    document.getElementById('h').value = Math.floor(
        (results.totalTime / 3600) % 24
    );
    document.getElementById('m').value = Math.floor(
        (results.totalTime / 60) % 60
    );
});

async function saveSettings(e) {
    const lookup = {
        time: saveTime,
    };

    await lookup[e.target.getAttribute('data-save')]();
}

async function saveTime() {
    const h = document.getElementById('h').value;
    const m = document.getElementById('m').value;

    const newTime = h * 3600 + m * 60;
    updateStatus('time', 'saved'); // TODO translate
    await sigmund.set('sync', { totalTime: newTime });
}

function fieldChanged(element) {
    updateStatus(element.getAttribute('data-modifies'), 'modified');
}

function updateStatus(name, status) {
    // Yes, you can probably inject something, but that shouldn't be a
    // problem.
    const applicableElement = document.querySelector(`[data-status="${name}"]`);

    applicableElement.textContent = status;
    applicableElement.setAttribute('data-current-status', status);
}

sigmund.handleEvent('storage updated', null, (changes, area) => {
    if (typeof changes.blocklist !== 'undefined') {
        synchronizeBlocklist(changes.blocklist.newValue);
    }

    if (typeof changes.notifyTimes !== 'undefined') {
        synchronizeNotifyTimes(changes.notifyTimes.newValue);
    }

    if (typeof changes.enableNotifications !== 'undefined') {
        synchronizeNotifySwitch(changes.enableNotifications.newValue);
    }
});

sigmund
    .get('sync', ['blocklist', 'notifyTimes', 'enableNotifications'])
    .then((results) => {
        let blocklist = Promise.resolve(results.blocklist);
        if (!Array.isArray(results.blocklist)) {
            blocklist = sigmund.set('sync', { blocklist: [] }).then(() => []);
        }

        let notifyTimes = Promise.resolve(results.notifyTimes);
        if (!Array.isArray(results.notifyTimes)) {
            notifyTimes = sigmund
                .set('sync', { notifyTimes: [] })
                .then(() => []);
        }

        const enableNotifications = Promise.resolve(
            results.enableNotifications
        );
        return Promise.all([blocklist, notifyTimes, enableNotifications]);
    })
    .then(([blocklist, notifyTimes, enableNotifications]) => {
        synchronizeBlocklist(blocklist);
        synchronizeNotifyTimes(notifyTimes);
        synchronizeNotifySwitch(enableNotifications);
    });

function synchronizeBlocklist(blocklist) {
    const toBlockList = document.querySelector('#to-block');

    const alreadyThere = [];
    for (const child of [...toBlockList.children]) {
        if (blocklist.indexOf(child.textContent) === -1) {
            child.remove();
        } else {
            alreadyThere.push(child.textContent);
        }
    }

    for (const entry of blocklist) {
        if (alreadyThere.indexOf(entry) === -1) {
            addBlockListOption(entry, toBlockList);
        }
    }
}

document.querySelector('#to-block').addEventListener('change', (e) => {
    if (e.target.selectedOptions.length > 0) {
        document.querySelector('#remove-host').removeAttribute('disabled');
    } else {
        document.querySelector('#remove-host').setAttribute('disabled', '');
    }
});

document.querySelector('#remove-host').addEventListener('click', (e) => {
    if (e.target.hasAttribute('disabled')) {
        return;
    }

    const list = document.querySelector('#to-block');
    const selected = [...list.selectedOptions].map((elem) => elem.textContent);

    sigmund.get('sync', ['blocklist']).then((results) => {
        const changed = results.blocklist.filter((entry) => {
            return selected.indexOf(entry) === -1;
        });

        return sigmund.set('sync', { blocklist: changed });
    });
});

document.querySelector('#block-form').addEventListener('submit', (e) => {
    e.preventDefault();

    if (e.submitter.id !== 'add-host') {
        return;
    }

    const input = document.querySelector('#block-form input#host');

    const newHost = input.value;
    if (newHost.length === 0) {
        return;
    }

    sigmund.get('sync', ['blocklist']).then((results) => {
        if (results.blocklist.indexOf(newHost) !== -1) {
            return;
        }

        results.blocklist.push(newHost);
        return sigmund.set('sync', { blocklist: results.blocklist });
    });

    input.value = '';
});

function addBlockListOption(host, toBlockList) {
    const element = document.createElement('option');
    element.textContent = host;

    if (!toBlockList) {
        toBlockList = document.querySelector('#to-block');
    }

    toBlockList.appendChild(element);
}

function synchronizeNotifySwitch(value) {
    const checkbox = document.querySelector('#enable-notifications');
    checkbox.checked = value;

    const editArea = document.querySelector('#notifications-edit');
    if (value) {
        editArea.removeAttribute('hidden');
    } else {
        editArea.setAttribute('hidden', 'hidden');
    }
}

document
    .querySelector('#enable-notifications')
    .addEventListener('input', (e) => {
        sigmund.set('sync', { enableNotifications: e.target.checked });
    });

function synchronizeNotifyTimes(notifyTimes) {
    const destination = document.querySelector('#notifications-rows');

    const alreadyThere = [];
    for (const child of [...destination.children]) {
        if (notifyTimes.indexOf(child.textContent) === -1) {
            child.remove();
        } else {
            alreadyThere.push(Number(child.getAttribute('milestone')));
        }
    }

    for (const milestone of notifyTimes) {
        if (alreadyThere.indexOf(milestone) === -1) {
            addNotifyTime(milestone, destination);
        }
    }
}

function addNotifyTime(milestone, destination) {
    const minutes = Math.floor(milestone / 60);
    const seconds = Math.floor(milestone % 60);

    const li = document.createElement('li');
    li.setAttribute('milestone', milestone.toString());
    li.classList.add('flex-row');

    const span = document.createElement('span');
    span.textContent = `${minutes}:${seconds.toString().padStart(2, '0')}`;
    li.appendChild(span);

    const removeButton = document.createElement('button');
    removeButton.textContent = sigmund.i18n('optionsNotificationsRemove');
    removeButton.addEventListener('click', () => removeNotifyTime(milestone));
    li.appendChild(removeButton);

    destination.appendChild(li);
}

function removeNotifyTime(milestone) {
    sigmund.get('sync', ['notifyTimes']).then((results) => {
        const index = results.notifyTimes.indexOf(milestone);
        if (index === -1) {
            return;
        }

        results.notifyTimes.splice(index, 1);
        return sigmund.set('sync', { notifyTimes: results.notifyTimes });
    });
}

document
    .querySelector('#notifications-form')
    .addEventListener('submit', (e) => {
        e.preventDefault();

        const whenElement = document.querySelector('#notifications-when');

        let when = whenElement.value;
        const unit = document
            .querySelector('#notifications-unit')
            .selectedOptions[0].getAttribute('name');

        if (unit === 'minutes') {
            when *= 60;
        }

        if (unit === 'hours') {
            when *= 3600;
        }

        sigmund.get('sync', ['notifyTimes']).then((results) => {
            if (results.notifyTimes.indexOf(when) !== -1) {
                return;
            }

            results.notifyTimes.push(when);
            return sigmund.set('sync', { notifyTimes: results.notifyTimes });
        });

        whenElement.value = '';
    });

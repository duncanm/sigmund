// Copyright 2022-2023 Duncan McIntosh <duncan82013@live.ca>
//
// This file is part of the Sigmund project.
// See <https://codeberg.org/duncanm/sigmund>.
//
// SPDX-License-Identifier: GPL-3.0-only
function SigmundWebExtensionTranslator() {
    /// Wraps over the new browser.storage.session API that isn't yet
    /// in Firefox. When it becomes available, we'll automatically
    /// switch over.
    function fixScope(scope) {
        if (scope !== 'session') {
            return scope;
        }

        return typeof browser.storage.session === 'undefined'
            ? 'local'
            : 'session';
    }

    this.get = function (scope, names) {
        return browser.storage[fixScope(scope)].get(names);
    };

    this.set = function (scope, contents) {
        return browser.storage[fixScope(scope)].set(contents);
    };

    this.handleEvent = function (name, params, handler) {
        if (name === 'extension restarted') {
            // We actually only run that at startup, so just run the
            // handler now.
            handler();
            return;
        }

        const lookup = {
            alarm: browser.alarms.onAlarm,
            'tab changed': browser.tabs.onActivated,
            'tab updated': browser.tabs.onUpdated,
            'tab removed': browser.tabs.onRemoved,
            'storage updated': browser.storage.onChanged,
        };

        lookup[name].addListener.apply(
            lookup[name],
            params === null ? [handler] : [handler, params]
        );
    };

    this.clearAlarm = function (name) {
        return browser.alarms.clear(name);
    };

    this.setAlarm = function (name, timeInSeconds) {
        return browser.alarms.create(name, {
            delayInMinutes: timeInSeconds / 60,
        });
    };

    this.currentSecondsSinceEpoch = function () {
        return Date.now() / 1000;
    };

    this.secondsSinceEpochAtTimeTomorrow = function (h, m, s) {
        const date = new Date();

        // JavaScript will automatically handle month/year woes.
        date.setDate(date.getDate() + 1);

        // Date.prototype.setHours appears to return the number of
        // milliseconds, which is (basically) exactly what we want.
        return date.setHours(h, m, s, 0) / 1000;
    };

    this.getTabInfoById = function (id) {
        return browser.tabs.get(id);
    };

    this.getActiveTabs = function () {
        return browser.tabs.query({
            active: true,
        });
    };

    this.navigateTab = function (tab, url) {
        return browser.tabs.update(tab | 0, { url });
    };

    this.getExtensionPage = function (relative) {
        return browser.runtime.getURL(relative);
    };

    this.showSettingsPage = function () {
        return browser.runtime.openOptionsPage();
    };

    this.setPopupIcon = function (name) {
        return browser.browserAction.setIcon({
            path: name,
        });
    };

    this.sendNotification = function (title, message, time) {
        return browser.notifications.create('', {
            type: 'basic',
            title,
            message,
            iconUrl: this.getExtensionPage('art/trespassing.svg'),
            eventTime: time,
        });
    };

    this.i18n = function (name, substitutions) {
        return browser.i18n.getMessage(name, substitutions);
    };
}

/* global sigmund:off */
const sigmund = new SigmundWebExtensionTranslator();

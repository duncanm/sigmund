// Copyright 2023 Duncan McIntosh <duncan82013@live.ca>
//
// This file is part of the Sigmund project.
// See <https://codeberg.org/duncanm/sigmund>.
//
// SPDX-License-Identifier: GPL-3.0-only
const test = require('tape');
const store = require('./wrap/background.js');

test('running out of time does not affect unblocked pages', async (t) => {
    t.plan(1);

    store.reset();
    store.tabs = ['https://a.example.edu', 'https://b.example.edu'];
    store.activeTabs = [0, 1];
    store.storage.sync = {
        blocklist: ['a.example.edu'],
        remaining: 0,
        expiry: sigmund.secondsSinceEpochAtTimeTomorrow(4, 0, 0),
    };

    await store.events['alarm'].handler({ name: 'tick' });
    t.deepEqual(store.tabs, [
        'x-test://src/block.html',
        'https://b.example.edu',
    ]);
});

test('running out of time redirects trespassing tabs', async (t) => {
    t.plan(1);

    store.reset();
    store.tabs = ['https://a.example.edu', 'https://b.example.edu'];
    store.activeTabs = [0, 1];
    store.storage.sync = {
        blocklist: ['a.example.edu', 'b.example.edu'],
        remaining: 0,
        expiry: sigmund.secondsSinceEpochAtTimeTomorrow(4, 0, 0),
    };

    await store.events['alarm'].handler({ name: 'tick' });
    t.deepEqual(store.tabs, [
        'x-test://src/block.html',
        'x-test://src/block.html',
    ]);
});

test('running out of time redirects trespassing tabs when running late', async (t) => {
    t.plan(1);

    store.reset();
    store.tabs = ['https://a.example.edu', 'https://b.example.edu'];
    store.activeTabs = [0, 1];
    store.storage.sync = {
        blocklist: ['a.example.edu', 'b.example.edu'],
        remaining: 1,
        expiry: sigmund.secondsSinceEpochAtTimeTomorrow(4, 0, 0),
    };
    store.storage.session = {
        lastTime: sigmund.currentSecondsSinceEpoch() - 2,
    };

    await store.events['alarm'].handler({ name: 'tick' });
    t.deepEqual(store.tabs, [
        'x-test://src/block.html',
        'x-test://src/block.html',
    ]);
});

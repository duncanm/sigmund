// Copyright 2022-2023 Duncan McIntosh <duncan82013@live.ca>
//
// This file is part of the Sigmund project.
// See <https://codeberg.org/duncanm/sigmund>.
//
// SPDX-License-Identifier: GPL-3.0-only
function SigmundBasicTestTranslator() {
    this.store = SigmundBasicTestTranslator.blankStore();
    const store = this.store;

    this.get = async function (scope, names) {
        let results = {};

        for (const name of names) {
            results[name] = store.storage[scope][name];
        }

        return results;
    };

    this.set = async function (scope, contents) {
        store.storage[scope] = Object.assign(store.storage[scope], contents);
    };

    this.handleEvent = function (name, params, handler) {
        store.events[name] = { params, handler };
    };

    this.getTabInfoById = async function (id) {
        return {
            url: store.tabs[id],
            id: id,
        };
    };

    this.setAlarm = async function (name, delay) {
        store.alarms[name] = delay;
    };

    this.clearAlarm = async function (name) {
        store.alarms[name] = null;
    };

    this.secondsSinceEpochAtTimeTomorrow = function () {
        return store.time + 50000;
    };

    this.currentSecondsSinceEpoch = function () {
        return store.time;
    };

    this.getActiveTabs = async function () {
        return Promise.all(store.activeTabs.map(this.getTabInfoById));
    };

    this.setPopupIcon = async function (name) {
        store.icon = name;
    };

    this.getExtensionPage = function (path) {
        return `x-test://${path}`;
    };

    this.navigateTab = async function (id, blockPageUrl) {
        store.tabs[id] = blockPageUrl;
    };

    this.sendNotification = async function (title, message, time) {
        store.notifications.push({
            title,
            message,
            time,
        });
    };

    this.i18n = function (name, substitutions) {
        let str = store.translations[name];

        if (str.indexOf('$1$') !== -1) {
            str = str.replace('$1$', substitutions[0]);
        }

        if (str.indexOf('$2$') !== -1) {
            str = str.replace('$2$', substitutions[1]);
        }

        return str;
    };
}

SigmundBasicTestTranslator.blankStore = function (store) {
    return {
        activeTabs: [],
        alarms: [],
        events: {},
        icon: 'art/all-good.svg',
        notifications: [],
        storage: {
            session: {},
            local: {},
            sync: {},
        },
        tabs: [],
        time: 0,
        translations: {},

        reset() {
            this.activeTabs = [];
            this.alarms = [];
            this.icon = 'art/all-good.svg';
            this.notifications = [];
            this.storage.session = {};
            this.storage.local = {};
            this.storage.sync = {};
            this.tabs = [];
            this.time = 0;
            this.translations = {};
        },
    };
};

module.exports = SigmundBasicTestTranslator;

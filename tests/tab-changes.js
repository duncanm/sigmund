// Copyright 2022 Duncan McIntosh <duncan82013@live.ca>
//
// This file is part of the Sigmund project.
// See <https://codeberg.org/duncanm/sigmund>.
//
// SPDX-License-Identifier: GPL-3.0-only
const test = require('tape');
const store = require('./wrap/background.js');

test('adding a non-trespassing tab does not create an alarm', async (t) => {
    t.plan(1);

    store.reset();
    store.tabs = ['https://google.com'];
    store.activeTabs = [0];

    await store.events['tab changed'].handler({
        previousTabId: -1,
        windowId: 0,
        tabId: 0,
    });

    t.false(store.alarms['tick']);
});

test('adding a trespassing tab creates an alarm', async (t) => {
    t.plan(1);

    store.reset();
    store.storage.sync = {
        totalTime: 500,
        remaining: null,
        blocklist: ['google.com'],
    };
    store.tabs = ['https://google.com'];
    store.activeTabs = [0];

    await store.events['tab changed'].handler({
        previousTabId: -1,
        windowId: 0,
        tabId: 0,
    });

    t.equal(store.alarms['tick'], 500);
});

test('removing the only trespassing tab clears the alarm', async (t) => {
    t.plan(1);

    store.reset();
    store.storage.sync = {
        totalTime: 500,
        remaining: null,
        blocklist: ['google.com'],
    };
    store.storage.session = {
        lastTime: 0,
    };
    store.tabs = ['https://google.com'];
    store.activeTabs = [];

    await store.events['tab changed'].handler(0, {});

    t.false(store.alarms['tick']);
});

test('changing from a trespassing tab clears the alarm', async (t) => {
    t.plan(2);

    store.reset();
    store.storage.sync = {
        totalTime: 500,
        remaining: null,
        blocklist: ['example.edu'],
    };
    store.tabs = ['https://example.edu', 'https://example.com'];
    store.activeTabs = [0];

    await store.events['tab changed'].handler({
        previousTabId: -1,
        windowId: 0,
        tabId: 0,
    });

    t.equal(store.alarms['tick'], 500);

    store.activeTabs = [1];
    await store.events['tab changed'].handler({
        previousTabId: 0,
        windowId: 0,
        tabId: 1,
    });

    t.false(store.alarms['tick']);
});

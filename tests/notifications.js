// Copyright 2023 Duncan McIntosh <duncan82013@live.ca>
//
// This file is part of the Sigmund project.
// See <https://codeberg.org/duncanm/sigmund>.
//
// SPDX-License-Identifier: GPL-3.0-only
const test = require('tape');
const store = require('./wrap/background.js');

const TRANSLATIONS = {
    notificationWarningTitle: 'title',
    notificationWarningBody: 'body $1$|$2$',
};

test('no notification is scheduled unless trespassing', async (t) => {
    t.plan(2);

    store.reset();
    store.tabs = ['https://a.example.edu'];
    store.activeTabs = [0];
    store.storage.sync = {
        totalTime: 500,
        remaining: null,
        blocklist: [],
        notifyTimes: [60, 120, 180],
    };

    await store.events['alarm'].handler({ name: 'tick' });
    t.false(store.alarms['notify']);
    t.equal(store.notifications.length, 0);
});

test('a notification is scheduled if trespassing', async (t) => {
    t.plan(2);

    store.reset();
    store.tabs = ['https://a.example.edu'];
    store.activeTabs = [0];
    store.storage.sync = {
        totalTime: 500,
        remaining: null,
        blocklist: ['a.example.edu'],
        notifyTimes: [60, 120, 180],
        enableNotifications: true,
    };

    await store.events['alarm'].handler({ name: 'tick' });
    t.equal(store.alarms['notify'], 500 - 180);
    t.equal(store.notifications.length, 0);
});

test('a notification is not scheduled if disabled', async (t) => {
    t.plan(2);

    store.reset();
    store.tabs = ['https://a.example.edu'];
    store.activeTabs = [0];
    store.storage.sync = {
        totalTime: 500,
        remaining: null,
        blocklist: ['a.example.edu'],
        notifyTimes: [60, 120, 180],
        enableNotifications: false,
    };

    await store.events['alarm'].handler({ name: 'tick' });
    t.false(store.alarms['notify']);
    t.equal(store.notifications.length, 0);
});

test('a notification is dispatched if time is reached', async (t) => {
    t.plan(4);

    store.reset();
    store.tabs = ['https://a.example.edu'];
    store.activeTabs = [0];
    store.translations = TRANSLATIONS;
    store.storage.sync = {
        totalTime: 180,
        remaining: null,
        blocklist: ['a.example.edu'],
        notifyTimes: [60, 120, 180],
        enableNotifications: true,
    };

    await store.events['alarm'].handler({ name: 'tick' });
    t.equal(store.alarms['notify'], 0);
    t.equal(store.notifications.length, 0);

    await store.events['alarm'].handler({ name: 'notify' });
    t.equal(Math.round(store.alarms['notify']), 60);
    t.deepEqual(store.notifications, [
        {
            title: 'title',
            message: 'body 3|00',
            time: store.time,
        },
    ]);
});

test('notifications have the correct time if they are delayed', async (t) => {
    t.plan(6);

    store.reset();
    store.tabs = ['https://a.example.edu'];
    store.activeTabs = [0];
    store.translations = TRANSLATIONS;
    store.storage.sync = {
        totalTime: 500,
        remaining: null,
        blocklist: ['a.example.edu'],
        notifyTimes: [60, 120, 180],
        enableNotifications: true,
    };

    await store.events['alarm'].handler({ name: 'tick' });
    t.equal(store.storage.sync.remaining, 500);
    t.equal(store.alarms['notify'], 500 - 180);
    t.equal(store.notifications.length, 0);

    store.time += 500 - 180 + 0.1;
    await store.events['alarm'].handler({ name: 'notify' });
    t.ok(store.storage.sync.remaining - (180 - 0.1) < 0.01);
    t.equal(Math.round(store.alarms['notify']), 60);
    t.deepEqual(store.notifications, [
        {
            title: 'title',
            message: 'body 3|00',
            time: store.time,
        },
    ]);
});

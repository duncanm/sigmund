// Copyright 2022 Duncan McIntosh <duncan82013@live.ca>
//
// This file is part of the Sigmund project.
// See <https://codeberg.org/duncanm/sigmund>.
//
// SPDX-License-Identifier: GPL-3.0-only
const SigmundBasicTestTranslator = require('../basic-translator');

globalThis.sigmund = new SigmundBasicTestTranslator();
module.exports = globalThis.sigmund.store;

require('../../src/background.js');

// Copyright 2022 Duncan McIntosh <duncan82013@live.ca>
//
// This file is part of the Sigmund project.
// See <https://codeberg.org/duncanm/sigmund>.
//
// SPDX-License-Identifier: GPL-3.0-only
const test = require('tape');
const store = require('./wrap/background.js');

test('changing to a trespassing tab sets the icon', async (t) => {
    t.plan(1);

    store.reset();
    store.tabs = ['https://example.com', 'https://example.edu'];
    store.activeTabs = [0];
    store.storage.sync = {
        totalTime: 500,
        remaining: null,
        blocklist: ['example.com'],
    };

    await store.events['tab changed'].handler({
        previousTabId: -1,
        windowId: 0,
        tabId: 0,
    });

    t.equal(store.icon, 'art/trespassing.svg');
});

test('changing from a trespassing tab sets the icon', async (t) => {
    t.plan(1);

    store.reset();
    store.tabs = ['https://example.com', 'https://example.edu'];
    store.storage.sync = {
        totalTime: 500,
        remaining: null,
        blocklist: ['example.com'],
    };

    store.activeTabs = [];
    await store.events['tab changed'].handler({
        previousTabId: -1,
        windowId: 0,
        tabId: 0,
    });

    store.activeTabs = [1];
    await store.events['tab changed'].handler({
        previousTabId: 0,
        windowId: 0,
        tabId: 1,
    });

    t.equal(store.icon, 'art/all-good.svg');
});

test('running out of time sets the icon', async (t) => {
    t.plan(1);

    store.reset();
    store.tabs = ['https://example.com', 'https://example.edu'];
    store.activeTabs = [0];
    store.storage.sync = {
        totalTime: 1,
        remaining: null,
        blocklist: ['example.com'],
        expiry: 100,
    };
    store.time = 0;

    await store.events['tab changed'].handler({
        previousTabId: -1,
        windowId: 0,
        tabId: 0,
    });

    store.time = 1;
    await store.events['alarm'].handler({
        name: 'tick',
    });

    t.equal(store.icon, 'art/out-of-time.svg');
});

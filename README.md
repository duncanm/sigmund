<!--
   - Copyright 2022 Duncan McIntosh <duncan82013@live.ca>
   -
   - This file is part of the Sigmund project.
   - See <https://codeberg.org/duncanm/sigmund>.
   -
   - SPDX-License-Identifier: CC0-1.0
   -->

# Sigmund

<img align="right" alt="The project logo, like a stop-sign." src="./art/logo.svg" width="128" height="128" role="presentation" />

If you struggle with wasting time on Reddit, news sites, or anywhere
else, you can use Sigmund to stop yourself. It lets you set a list of
sites that should be blocked, so you'll only be able to use them for a
certain length of time per day.

Sigmund was originally a fork of the
[Monastery](https://github.com/omivore/monastery) addon, however right
when I finished a refactoring I realized there wasn't actually a
license, so I can't legally share my changes. :(

Sigmund itself is licensed under the GNU General Public License,
version 3.0. The translations and documentation are licensed under the
Creative Commons Zero license.

Contributions are welcome!
